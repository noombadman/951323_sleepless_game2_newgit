﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BackMenu : MonoBehaviour
{
    [SerializeField] Button _backButton;
    [SerializeField] Button _nextHTP;
    [SerializeField] Button _backHTP;
    // Start is called before the first frame update
    void Start()
    {
        _backButton.onClick.AddListener
       (delegate { BackToMainMenuButtonClick(_backButton); });
        _nextHTP.onClick.AddListener
            (delegate { nextHTP(_nextHTP); });
        _backHTP.onClick.AddListener
            (delegate { BackHTP(_backHTP); });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void BackToMainMenuButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneMainMenu");
    }
    public void BackHTP(Button button)
    {
        SceneManager.LoadScene("SceneHowToPlay");
    }
    public void nextHTP(Button button)
    {
        SceneManager.LoadScene("SceneHowToPlay2");
    }
}
