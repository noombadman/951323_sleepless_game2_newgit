﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomsizeZombieAppecrance : MonoBehaviour
{
    public Material[] zombieMateerials;
    // Start is called before the first frame update
    void Start()
    {
        SkinnedMeshRenderer myRanderer = GetComponent<SkinnedMeshRenderer>();
        myRanderer.material = zombieMateerials[Random.Range(0,zombieMateerials.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
