﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public GameObject[] weapons;
    bool[] weaponAvailable;
    public Image weaponImage;

    int currantWeapon;

    // Start is called before the first frame update
    void Start()
    {
        weaponAvailable = new bool[weapons.Length];
        for (int i = 0; i < weapons.Length; i++) weaponAvailable[i] = false;
        currantWeapon = 0;
        //weaponAvailable[currantWeapon] = true;
        for (int i = 0; i < weapons.Length; i++) weaponAvailable[i] = true;

        deactvateWeapons();

        setWeaponActive(currantWeapon);

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("E key was pressed.");
            int i;
            for(i = currantWeapon+1; i < weapons.Length; i++ )
            {
                if(weaponAvailable[i] == true)
                {
                    currantWeapon = i;
                    setWeaponActive(currantWeapon);
                    return;
                }
            }
            for(i = 0; i < currantWeapon; i++)
            {
                if (weaponAvailable[i] == true)
                {
                    currantWeapon = i;
                    setWeaponActive(currantWeapon);
                    return;
                }
            }
        }
    }
    public void setWeaponActive(int whichWeapon)
    {
        if (!weaponAvailable[whichWeapon]) return;
        deactvateWeapons();
        weapons[whichWeapon].SetActive(true);
        weapons[whichWeapon].GetComponentInChildren<FireBullet>().initalizeWeapon();
    }

    void deactvateWeapons()
    {
        for (int i = 0; i < weapons.Length; i++) weapons[i].SetActive(false);
    }
    
    public void activateWeapon( int whichWeapon)
    {
        weaponAvailable[whichWeapon] = true;
    }
}
