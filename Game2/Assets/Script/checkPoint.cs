﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkPoint : MonoBehaviour
{
    
    void OnTriggerEnter(Collider other){
        if(other.gameObject.tag == "Player"){
            checkPointable checkpointable = other.GetComponent<checkPointable>();
            checkpointable.currentCheckpoint = this.transform ;
        }
    }
}
