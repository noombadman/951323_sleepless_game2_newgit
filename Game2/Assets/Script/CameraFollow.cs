﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform traget;
    public float smoothing = 5f;

    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - traget.position;  
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 tragetCamPos = traget.position + offset;

        transform.position = Vector3.Lerp(transform.position,tragetCamPos,smoothing * Time.deltaTime);
    }
}
