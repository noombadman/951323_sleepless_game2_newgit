﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainmenuController : MonoBehaviour
{
    [SerializeField] Button buttonStart;
    [SerializeField] Button HowtoPlay;
    [SerializeField] Button buttonCredit;
    [SerializeField] Button buttonExit;
    // Start is called before the first frame update
    void Start()
    {
        buttonStart.onClick.AddListener(delegate { StartButtonClick(buttonStart); });
        HowtoPlay.onClick.AddListener(delegate { HTPButtonClick(HowtoPlay); });
        buttonCredit.onClick.AddListener(delegate { CreditButtonClick(buttonCredit); });
        buttonExit.onClick.AddListener(delegate { ExitButtonClick(buttonExit); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("Scenegameplay1");
    }
    public void HTPButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneHowToPlay");
    }
    public void CreditButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneCredit");
    }
    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }
}
